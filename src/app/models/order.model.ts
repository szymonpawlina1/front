export interface OrderResponseModel {
    id: Number,
    service_name: String,
    service_price: Number,
    model_name: String,
    producent_name: String,
    finished: Boolean,
    create_date: Date,
    finished_date: Date,
}