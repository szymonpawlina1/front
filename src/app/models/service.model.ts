export interface ServiceModelServer {
    id: Number;
    repair: String;
    price: Number
}

export interface serviceServerResponse {
    count: number;
    services: ServiceModelServer[]
};
