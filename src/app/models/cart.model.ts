import { ProductModelServer } from "./product.model";
import { ServiceModelServer } from './service.model';

export interface CartModelServer {
  total: Number;
  data: [{
    product: ServiceModelServer,
    numInCart: Number
  }];
}

export interface CartModelPublic {
  total: Number;
  prodData: [{
    id: Number,
    incart: Number
  }]
}
