import { Component, OnInit } from '@angular/core';
import { ProductService } from "../../services/product.service";
import { ProductModelServer, serverResponse } from "../../models/product.model";
import { CartService } from "../../services/cart.service";
import { Router } from "@angular/router";
import { ServiceModelServer, serviceServerResponse } from 'src/app/models/service.model';
import { ThrowStmt } from '@angular/compiler';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PhoneModel } from 'src/app/models/phone.model';

@Component({
  selector: 'mg-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {
  products: ProductModelServer[] = [];
  services;
  dropDownForm: FormGroup;
  OrderForm: FormGroup;
  models: PhoneModel;
  order: any
  servicesList;


  constructor(private productService: ProductService,
    private cartService: CartService,
    private router: Router,
    private fb: FormBuilder) {
  }
  //dodac telefon i serwisy
  ngOnInit() {
    //  this.getServices()
    this.getModels()
    this.createDropDownForm()

  }

  createDropDownForm() {
    this.dropDownForm = this.fb.group({
      model: ['0'],
      service: ['0'],
      comments: ['']
    });
  }
  createOrderForm() {
    this.OrderForm = this.fb.group({
      //user_id zalogowanego trzebab edzie pobrac z cahce
      // user_id: this.userData.user_id,
      id: parseInt(this.dropDownForm.controls.service.value),
      comments: this.dropDownForm.controls.comments.value
    })
  }


  // getServices() {
  //   this.productService.getAllServices().subscribe((srvs: ServiceModelServer) => {
  //     this.services = srvs;
  //     console.log(this.services)
  //   })
  // }
  // getModels() {
  //   this.productService.getAllModel().subscribe((models: PhoneModel) => {
  //     this.models = models;
  //     console.log(this.models)
  //   })
  // }
  getModels() {
    this.productService.getAllModel().subscribe((models: PhoneModel) => {
      this.models = models;
      console.log(this.models)
    })
  }
  // getServiceById() {
  //   this.productService.getAllServicesofModel().subscribe(servicesList => {
  //     this.servicesList = servicesList;
  //     console.log(this.servicesList)
  //   })
  // }

  getServiceById(event) {
    this.productService.getAllServicesofModel(Number(event.target.value)).
      subscribe(servicesList => {
        this.servicesList = servicesList;
        console.log(this.servicesList)
      })
  }

  AddProduct() {
    this.createOrderForm();
    this.order = this.OrderForm.value
    console.log(this.order)
    let id = this.order.id
    //id = this.OrderForm.controls.services.value
    // console.log(id)
    this.cartService.AddProductToCart(id);
  }
}
