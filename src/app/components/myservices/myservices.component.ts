import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { OrderResponseModel } from 'src/app/models/order.model';
import { ServiceModelServer } from 'src/app/models/service.model';
import { MessageService } from 'src/app/services/message.service';
import { OrderService } from 'src/app/services/order.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'mg-myservices',
  templateUrl: './myservices.component.html',
  styleUrls: ['./myservices.component.scss']
})
export class MyservicesComponent implements OnInit {
  // order: OrderResponseModel[];
  order

  id;
  messagesList;
  service: ServiceModelServer;
  data: any;
  formMessage: FormGroup;

  constructor(
    private orderService: OrderService,
    private messageService: MessageService,
    private productService: ProductService,
    private router: Router,
    private fb: FormBuilder,
    private toast: ToastrService


  ) { }

  ngOnInit(): void {
    this.getOrder()
    // this.getMessages()
    // this.getService(1)
    this.createForm()

  }

  getOrder() {
    this.orderService.getOrderByUserId()
      .then((order) => {
        this.order = order
        console.log(this.order)
      })
  }

  // getService(id) {
  //   this.productService.getSingleService(id).subscribe(service => {
  //     this.service = service;
  //     console.log(this.service)
  //   })
  // }
  createForm() {
    this.formMessage = this.fb.group({
      messages_text: ['']
    })
  }


  getMessages(id) {
    console.log(id)
    this.messageService.getMessagesByOrderId(id).subscribe(messagesList => {
      this.messagesList = messagesList[0].communication;
      // this.id = this.messagesList[0].communications
      console.log(this.messagesList)

    })

  }
  sendMessage() {
    // console.log(this.formMessage.value)
    // let message = this.formMessage.controls.messages_text.value
    // console.log(message)
    // this.getMessages(this.id)

    console.log(this.id)
    this.data = {
      communications: 3,
      messages_text: this.formMessage.controls.messages_text.value,
      // messages_text: 'asd',

      owner: 1
    }
    this.messageService.sendMessageToOrder(this.data).subscribe(() =>



      this.toast.success(`Wysłano wiadomość`, "", {
        timeOut: 1500,
        progressBar: true,
        progressAnimation: 'increasing',
        positionClass: 'toast-top-right'
      })

    )
  }

}
