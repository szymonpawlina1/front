import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class MessageService {
    ServerURL = environment.serverURL;

    constructor(private http: HttpClient) {
    }
    getMessagesByOrderId(id: Number) {
        return this.http.get('http://127.0.0.1:8000/communication/order/' + id + '/')
    }
    sendMessageToOrder(data: any) {
        return this.http.post('http://127.0.0.1:8000/communication/create/', data)
    }
}
