import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { OrderResponseModel } from '../models/order.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  ServerURL = environment.serverURL;

  constructor(private http: HttpClient) {
  }
  //DODAC KONKRENTEGO USERA PO ZALOGOWANIU
  getOrderByUserId() {
    return this.http.get<OrderResponseModel[]>('http://127.0.0.1:8000/orders/user/2/').toPromise();
  }
}

