import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
import { ProductModelServer, serverResponse } from "../models/product.model";
import { ServiceModelServer, serviceServerResponse } from '../models/service.model';
import { PhoneModel } from '../models/phone.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private url = environment.serverURL;

  constructor(private http: HttpClient) {
  }


  //tutaj dodac wszystkie serwisy ale dla konkretnego modelu 
  // getAllServices(): Observable<ServiceModelServer> {
  //   return this.http.get<ServiceModelServer>('http://127.0.0.1:8000/repairs/model/1/');
  // }
  getSingleService(id: Number): Observable<ServiceModelServer> {
    return this.http.get<ServiceModelServer>('http://127.0.0.1:8000/repairs/repair/' + id)
  }
  getAllModel(): Observable<PhoneModel> {
    return this.http.get<PhoneModel>('http://localhost:8000/repairs/models/')
    // return this.http.get<PhoneModel>(this.url + 'repairs/')
  }
  getAllServicesofModel(id: Number) {
    return this.http.get('http://127.0.0.1:8000/repairs/repairs/' + id)
  }
  // getAllServicesofModel() {
  //   return this.http.get('http://127.0.0.1:8000/repairs/model/')
  // }



  /////////////////////////////////////////////////////////////////////
  getAllProducts(): Observable<serverResponse> {
    return this.http.get<serverResponse>(this.url + 'models');
  }

  getSingleProduct(id: Number): Observable<ProductModelServer> {
    return this.http.get<ProductModelServer>(this.url + 'models');
  }

  getProductsFromCategory(catName: String): Observable<ProductModelServer[]> {
    return this.http.get<ProductModelServer[]>(this.url + '');
  }

}
